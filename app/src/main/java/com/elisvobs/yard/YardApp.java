package com.elisvobs.yard;

import android.app.Application;

import com.google.firebase.FirebaseApp;

public class YardApp extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        com.google.firebase.FirebaseApp.initializeApp(getApplicationContext());
    }
}
package com.elisvobs.yard;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class User {
    public String name;
    public String age;
    public String gender;
    public String idNumber;
    public String occupation;
    public String education;
    public String village;
    public String ward;
    public String constituency;
    public String district;
    public String province;
    public String email;
    public String phone;

    public User() {
    }

    public User(String name, String age, String gender, String idNumber, String occupation, String education, String village, String ward, String constituency, String district, String province, String email, String phone) {
        this.name = name;
        this.age = age;
        this.gender = gender;
        this.idNumber = idNumber;
        this.occupation = occupation;
        this.education = education;
        this.village = village;
        this.ward = ward;
        this.constituency = constituency;
        this.district = district;
        this.province = province;
        this.email = email;
        this.phone = phone;
    }
}